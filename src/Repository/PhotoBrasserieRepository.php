<?php

namespace App\Repository;

use App\Entity\PhotoBrasserie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PhotoBrasserie|null find($id, $lockMode = null, $lockVersion = null)
 * @method PhotoBrasserie|null findOneBy(array $criteria, array $orderBy = null)
 * @method PhotoBrasserie[]    findAll()
 * @method PhotoBrasserie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhotoBrasserieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PhotoBrasserie::class);
    }

    // /**
    //  * @return PhotoBrasserie[] Returns an array of PhotoBrasserie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PhotoBrasserie
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
