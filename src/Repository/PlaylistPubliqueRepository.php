<?php

namespace App\Repository;

use App\Entity\PlaylistPublique;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlaylistPublique|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlaylistPublique|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlaylistPublique[]    findAll()
 * @method PlaylistPublique[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaylistPubliqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlaylistPublique::class);
    }

    // /**
    //  * @return PlaylistPublique[] Returns an array of PlaylistPublique objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlaylistPublique
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
