<?php

namespace App\Repository;

use App\Entity\Saveur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Saveur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Saveur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Saveur[]    findAll()
 * @method Saveur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SaveurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Saveur::class);
    }

    // /**
    //  * @return Saveur[] Returns an array of Saveur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Saveur
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
