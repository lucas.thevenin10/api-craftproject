<?php

namespace App\Repository;

use App\Entity\TempsFort;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TempsFort|null find($id, $lockMode = null, $lockVersion = null)
 * @method TempsFort|null findOneBy(array $criteria, array $orderBy = null)
 * @method TempsFort[]    findAll()
 * @method TempsFort[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TempsFortRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TempsFort::class);
    }

    // /**
    //  * @return TempsFort[] Returns an array of TempsFort objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TempsFort
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
