<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Entity\Utilisateur;
class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login", methods="POST")
     */
    public function login(Request $request): Response
    {
        $mail = $request->get('mail');
        $result = $this->getDoctrine()->getRepository(Utilisateur::class)->findOneBy(['mail'=>$mail]);
        $mdp = $request->get('password');
        if(password_verify($mdp,$result->getPassword())){
            return new Response(json_encode($result->getId()));
        }
        else {
            return new Response('erreur pour '.$mail.' et '.$password);
        }
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
