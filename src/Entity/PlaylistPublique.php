<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PlaylistPubliqueRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *      normalizationContext={"groups"={"pp:read"}},
 *      denormalizationContext={"groups"={"pp:write"}}
 * )
 * @ORM\Entity(repositoryClass=PlaylistPubliqueRepository::class)
 * @ORM\Table(name="PlaylistPublique")
 */
class PlaylistPublique
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="Id_PlaylistPublique")
     * @Groups({"pp:read","pp:write"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, name="Nom_PlaylistPublique")
     * @Groups({"pp:read","pp:write"})
     */
    private $nom;

    /**
    * @ORM\Column(type="string", length=255, name="UrlImage_PlaylistPublique")
    * @Groups({"pp:read","pp:write"})
    */
   private $image;
   
    /**
     * @ORM\ManyToMany(targetEntity=Biere::class)
     * @ORM\JoinTable(name="Lister",
     *    joinColumns={@ORM\JoinColumn(name="Id_PlaylistPublique", referencedColumnName="Id_PlaylistPublique")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="Gencode_Biere", referencedColumnName="Gencode_Biere")}
     *    )
     * @Groups({"pp:read","pp:write"})
     */
    private $bieres;

    public function __construct()
    {
        $this->bieres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }
    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|Biere[]
     */
    public function getBieres(): Collection
    {
        return $this->bieres;
    }

    public function addBiere(Biere $biere): self
    {
        if (!$this->bieres->contains($biere)) {
            $this->bieres[] = $biere;
        }

        return $this;
    }

    public function removeBiere(Biere $biere): self
    {
        $this->bieres->removeElement($biere);

        return $this;
    }
}
