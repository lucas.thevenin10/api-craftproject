<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PhotoBrasserieRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=PhotoBrasserieRepository::class)
 * @ORM\Table(name="PhotoBrasserie")
 */
class PhotoBrasserie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="Id_PhotoBrasserie")
     * @Groups("brasserie:read","brasserie:write")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Brasserie::class, inversedBy="photoBrasseries",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false, name="Id_Brasserie", referencedColumnName="Id_Brasserie")
     * @ORM\JoinColumn(nullable=false)
     */
    private $brasserie;

    /**
     * @ORM\Column(type="string", length=255, name="Url_PhotoBrasserie")
     * @Groups("brasserie:read","brasserie:write")
     */
    private $url;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBrasserie(): ?Brasserie
    {
        return $this->brasserie;
    }

    public function setBrasserie(?Brasserie $brasserie): self
    {
        $this->brasserie = $brasserie;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }
}
