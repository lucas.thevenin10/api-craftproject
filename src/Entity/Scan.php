<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ScanRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *   normalizationContext={"groups"={"scan:read"}},
 *     denormalizationContext={"groups"={"scan:write"}})
 * @ORM\Entity(repositoryClass=ScanRepository::class)
 * @ORM\Table(name="Scan")
 */
class Scan
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="Id_Scan")
     * @Groups({"scan:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", name="DateHeure_Scan")
     * @Groups({"scan:read","user:read"})
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity=Biere::class)
     * @ORM\JoinColumn(nullable=false, name="Gencode_Biere", referencedColumnName="Gencode_Biere")
     * @Groups({"scan:write","scan:read","user:read"})
     */
    private $biere;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class, inversedBy="scans")
     * @ORM\JoinColumn(nullable=false, name="Id_Utilisateur", referencedColumnName="Id_Utilisateur")
     * @Groups({"scan:write","scan:read"})
     */
    private $utilisateur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getBiere(): ?Biere
    {
        return $this->biere;
    }

    public function setBiere(?Biere $biere): self
    {
        $this->biere = $biere;

        return $this;
    }

    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?Utilisateur $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }
}
