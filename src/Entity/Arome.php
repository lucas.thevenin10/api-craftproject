<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AromeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=AromeRepository::class)
 * @ORM\Table(name="Arome")
 */
class Arome
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="Id_Arome")
     * @Groups("biere:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, name="Nom_Arome")
     * @Groups("biere:read")
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, name="Picto_Arome")
     * @Groups("biere:read")
     */
    private $urlPicto;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getUrlPicto(): ?string
    {
        return $this->urlPicto;
    }

    public function setUrlPicto(string $urlPicto): self
    {
        $this->urlPicto = $urlPicto;

        return $this;
    }
}
