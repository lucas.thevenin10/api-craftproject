<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AdministrateurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\SerializedName;
/**
 * @ORM\Entity(repositoryClass=AdministrateurRepository::class)
 * @ORM\Table(name="Administrateur")
 * @ApiResource(
 *     normalizationContext={"groups"={"admin:read"}},
 *     denormalizationContext={"groups"={"admin:write"}}
 * )
 */
class Administrateur implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="Id_Administrateur")
     * @Groups({"admin:read","admin:write"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, name="Pseudo_Administrateur")
     * @Groups({"admin:read","admin:write"})
     */
    private $username;

    /**
     * @Groups({"admin:write"})
     * @SerializedName("password")
     */
    private $plainPassword;

    /**
     *  @ORM\Column(type="string", length=255, name="Mdp_Administrateur")
     */
    private $password;

    private $roles = [];

    private $salt;

    /**
     * @Groups({"admin:write"})
     * @ORM\Column(type="string", length=255, name="AuthToken_Administrateur")
     */
    private $token;

    /**
     * @ORM\OneToMany(targetEntity=Biere::class, mappedBy="administrateur")
     */
    private $bieres;

    public function __construct()
    {
        $this->bieres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }   

    public function getRoles()
    {
        return $this->roles;
        $roles[]='ROLE_USER';

    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }
    
    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }


    /**
     * @return Collection|Biere[]
     */
    public function getBieres(): Collection
    {
        return $this->bieres;
    }

    public function addBiere(Biere $biere): self
    {
        if (!$this->bieres->contains($biere)) {
            $this->bieres[] = $biere;
            $biere->setAdministrateur($this);
        }

        return $this;
    }

    public function removeBiere(Biere $biere): self
    {
        if ($this->bieres->removeElement($biere)) {
            // set the owning side to null (unless already changed)
            if ($biere->getAdministrateur() === $this) {
                $biere->setAdministrateur(null);
            }
        }

        return $this;
    }

    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }
}
