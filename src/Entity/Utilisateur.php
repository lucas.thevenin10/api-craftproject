<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UtilisateurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\SerializedName;
/**
 * @ApiResource(
 *      normalizationContext={"groups"={"user:read"}},
 *     denormalizationContext={"groups"={"user:write"}}
 * )
 * @ORM\Entity(repositoryClass=UtilisateurRepository::class)
 * @ORM\Table(name="Utilisateur")
 */
class Utilisateur implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="Id_Utilisateur")
     * @Groups({"user:read","user:write"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, name="Nom_Utilisateur")
     *  @Groups({"user:read","user:write"})
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, name="Prenom_Utilisateur")
     *  @Groups({"user:read","user:write"})
     */
    private $prenom;

    /**
     * @ORM\Column(type="datetime", name="DateNaissance_Utilisateur")
     *  @Groups({"user:read","user:write"})
     */
    private $dateNaissance;

    /**
     * @ORM\OneToMany(targetEntity=Playlist::class, mappedBy="utilisateur", orphanRemoval=true)
     *  @Groups({"user:read","user:write"})
     */
    private $playlists;

    /**
     * @ORM\OneToMany(targetEntity=Scan::class, mappedBy="utilisateur")
     *  @Groups({"user:read","user:write"})
     */
    private $scans;

    /**
     * @ORM\Column(type="string", length=255, name="Mail_Utilisateur")
     * @Groups({"user:read","user:write"})
     */
    private $mail;

    /**
     * @SerializedName("password")
     *  @Groups({"user:write"})
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="string", length=255, name="Mdp_Utilisateur")
     */
    private $password;

    private $salt;

    private $roles = [];

    public function __construct()
    {
        $this->playlists = new ArrayCollection();
        $this->scans = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    public function setDateNaissance(\DateTimeInterface $dateNaissance): self
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    /**
     * @return Collection|Playlist[]
     */
    public function getPlaylists(): Collection
    {
        return $this->playlists;
    }

    public function addPlaylist(Playlist $playlist): self
    {
        if (!$this->playlists->contains($playlist)) {
            $this->playlists[] = $playlist;
            $playlist->setUtilisateur($this);
        }

        return $this;
    }

    public function removePlaylist(Playlist $playlist): self
    {
        if ($this->playlists->removeElement($playlist)) {
            // set the owning side to null (unless already changed)
            if ($playlist->getUtilisateur() === $this) {
                $playlist->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Scan[]
     */
    public function getScans(): Collection
    {
        return $this->scans;
    }

    public function addScan(Scan $scan): self
    {
        if (!$this->scans->contains($scan)) {
            $this->scans[] = $scan;
            $scan->setUtilisateur($this);
        }

        return $this;
    }

    public function removeScan(Scan $scan): self
    {
        if ($this->scans->removeElement($scan)) {
            // set the owning side to null (unless already changed)
            if ($scan->getUtilisateur() === $this) {
                $scan->setUtilisateur(null);
            }
        }

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getSalt(): ?string
    {
        return $this->salt;
    }

    public function setSalt(string $salt): self
    {
        $this->salt = $salt;

        return $this;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
        $roles[]='ROLE_USER';
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getUsername():string{
        return $this->mail;
    }

    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }
}
