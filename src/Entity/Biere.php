<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use App\Repository\BiereRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * @ApiResource(
 *      normalizationContext={"groups"={"biere:read"}},
 *      denormalizationContext={"groups"={"biere:write"}}
 * )
 * @ApiFilter(
 *  SearchFilter::class,properties = {
 *   "nom":"partial",
 *   "brasserie.nom":"partial",
 *   "couleur":"exact",
 *    "brasserie.origine.id"
 *  })
 * @ApiFilter(
 *  RangeFilter::class,properties = {
 *   "alcool"
 *  })
 * 
 * @ApiFilter(
 *  BooleanFilter::class,properties = {
 *   "bio"
 *  })
 * 
 * @ORM\Entity(repositoryClass=BiereRepository::class)
 * @ORM\Table(name="Biere")
 */ 

class Biere
{

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint", name="Gencode_Biere")
     * @Groups({"biere:read","biere:write","brasserie:read", "tf:read", "pp:read", "playlist:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, name="Nom_Biere")
     * @Groups({"biere:read","biere:write", "tf:read","brasserie:read", "pp:read"})
     */
    private $nom;

    /**
     * @ORM\Column(type="float", name="DegresAlcool_Biere")
     * @Groups({"biere:read","biere:write"})
     */
    private $alcool;

    /**
     * @ORM\Column(type="string", length=255, name="StyleDetail_Biere")
     * @Groups({"biere:read","biere:write"})
     */
    private $styleDetail;

    /**
     * @ORM\Column(type="string", length=255, name="Buvabilite_Biere")
     * @Groups({"biere:read","biere:write"})
     */
    private $buvabilite;

    /**
     * @ORM\Column(type="string", length=255, name="Degustation_Biere")
     * @Groups({"biere:read","biere:write"})
     */
    private $degustation;

    /**
     * @ORM\ManyToOne(targetEntity=Brasserie::class, inversedBy="bieres")
     * @ORM\JoinColumn(nullable=false, name="Id_Brasserie", referencedColumnName="Id_Brasserie")
     * @Groups({"biere:read","biere:write","tf:read", "pp:read"})
     */
    private $brasserie;

    /**
     * @ORM\ManyToOne(targetEntity=Style::class, inversedBy="bieres")
     * @ORM\JoinColumn(nullable=false, name="Id_Style", referencedColumnName="Id_Style")
     * @Groups({"biere:read","biere:write"})
     */
    private $style;

    /**
     * @ORM\ManyToMany(targetEntity=Plat::class)
     * @ORM\JoinTable(name="AccorderSpecifiquement",
     *    joinColumns={@ORM\JoinColumn(name="Gencode_Biere", referencedColumnName="Gencode_Biere")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="Id_Plat", referencedColumnName="Id_Plat")}
     *    )
     * @Groups({"biere:read","biere:write"})
     */
    private $plats;

    /**
     * @ORM\ManyToMany(targetEntity=Arome::class)
     * @ORM\JoinTable(name="AssocierArome",
     *    joinColumns={@ORM\JoinColumn(name="Gencode_Biere", referencedColumnName="Gencode_Biere")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="Id_Arome", referencedColumnName="Id_Arome")}
     *    )
     * @Groups({"biere:read","biere:write"})
     */
    private $aromes;

    /**
     * @ORM\Column(type="boolean", nullable=true, name="Bio_Biere")
     * @Groups({"biere:read","biere:write"})
     */
    private $bio;

    /**
     * @ORM\Column(type="string", length=255, name="Couleur_Biere")
     * @Groups({"biere:read","biere:write","brasserie:read"})
     */
    private $couleur;

    /**
     * @ORM\Column(type="string", length=255, name="OrigineVille_Biere")
     * @Groups({"biere:read","biere:write"})
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=255, name="UrlImage_Biere")
     * @Groups({"biere:read","brasserie:read","biere:write"})
     */
    private $image;

    /**
     * @ORM\Column(type="text", name="Conservation_Biere")
     * @Groups({"biere:read","biere:write"})
     */
    private $conservation;

    /**
     * @ORM\ManyToOne(targetEntity=Administrateur::class, inversedBy="bieres")
     * @ORM\JoinColumn(nullable=false, name="Id_Administrateur", referencedColumnName="Id_Administrateur")
     * @Groups({"biere:read","biere:write"})
     */
    private $administrateur;

    /**
     * @ORM\ManyToMany(targetEntity=Saveur::class, inversedBy="bieres")
     * @ORM\JoinTable(name="AssocierSaveur",
     *    joinColumns={@ORM\JoinColumn(name="Gencode_Biere", referencedColumnName="Gencode_Biere")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="Id_Saveur", referencedColumnName="Id_Saveur")}
     *    )
     * @Groups({"biere:read","biere:write"})
     */
    private $saveurs;

    public function __construct()
    {
        $this->plats = new ArrayCollection();
        $this->aromes = new ArrayCollection();
        $this->saveurs = new ArrayCollection();
    }


    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAlcool(): ?float
    {
        return $this->alcool;
    }

    public function setAlcool(float $alcool): self
    {
        $this->alcool = $alcool;

        return $this;
    }

    public function getStyleDetail(): ?string
    {
        return $this->styleDetail;
    }

    public function setStyleDetail(string $styleDetail): self
    {
        $this->styleDetail = $styleDetail;

        return $this;
    }

    public function getBuvabilite(): ?string
    {
        return $this->buvabilite;
    }

    public function setBuvabilite(string $buvabilite): self
    {
        $this->buvabilite = $buvabilite;

        return $this;
    }

    public function getDegustation(): ?string
    {
        return $this->degustation;
    }

    public function setDegustation(string $degustation): self
    {
        $this->degustation = $degustation;

        return $this;
    }

    public function getBrasserie(): ?Brasserie
    {
        return $this->brasserie;
    }

    public function setBrasserie(?Brasserie $brasserie): self
    {
        $this->brasserie = $brasserie;

        return $this;
    }

    public function getStyle(): ?Style
    {
        return $this->style;
    }

    public function setStyle(?Style $style): self
    {
        $this->style = $style;
        return $this;
    }

    /**
     * @return Collection|Plat[]
     */
    public function getPlats(): Collection
    {
        return $this->plats;
    }

    public function addPlat(Plat $plat): self
    {
        if (!$this->plats->contains($plat)) {
            $this->plats[] = $plat;
        }

        return $this;
    }

    public function removePlat(Plat $plat): self
    {
        $this->plats->removeElement($plat);

        return $this;
    }

    /**
     * @return Collection|Arome[]
     */
    public function getAromes(): Collection
    {
        return $this->aromes;
    }

    public function addArome(Arome $arome): self
    {
        if (!$this->aromes->contains($arome)) {
            $this->aromes[] = $arome;
        }

        return $this;
    }

    public function removeArome(Arome $arome): self
    {
        $this->aromes->removeElement($arome);

        return $this;
    }

    public function getBio(): ?bool
    {
        return $this->bio;
    }

    public function setBio(?bool $bio): self
    {
        $this->bio = $bio;

        return $this;
    }

    public function getCouleur(): ?string
    {
        return $this->couleur;
    }

    public function setCouleur(string $couleur): self
    {
        $this->couleur = $couleur;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getConservation(): ?string
    {
        return $this->conservation;
    }

    public function setConservation(string $conservation): self
    {
        $this->conservation = $conservation;

        return $this;
    }

    public function getAdministrateur(): ?Administrateur
    {
        return $this->administrateur;
    }

    public function setAdministrateur(?Administrateur $administrateur): self
    {
        $this->administrateur = $administrateur;

        return $this;
    }

    /**
     * @return Collection|Saveur[]
     */
    public function getSaveurs(): Collection
    {
        return $this->saveurs;
    }

    public function addSaveur(Saveur $saveur): self
    {
        if (!$this->saveurs->contains($saveur)) {
            $this->saveurs[] = $saveur;
        }

        return $this;
    }

    public function removeSaveur(Saveur $saveur): self
    {
        $this->saveurs->removeElement($saveur);

        return $this;
    }
}
