<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ReponseRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 *  @ApiResource(
 *      normalizationContext={"groups"={"rep:read"}},
 *      denormalizationContext={"groups"={"rep:write"}})
 * @ORM\Entity(repositoryClass=ReponseRepository::class)
 * @ORM\Table(name="Reponse")
 */
class Reponse
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="Id_Reponse")
     * @Groups({"qst:read","rep:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"qst:read","qst:write","rep:read","rep:write"})
     */
    private $Intitule;

    /**
     * @ORM\ManyToOne(targetEntity=Question::class, inversedBy="reponses",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false, name="relation", referencedColumnName="Id_Question")
     */
    private $relation;

    /**
     * @ORM\ManyToOne(targetEntity=Style::class)
     * @ORM\JoinColumn(nullable=false, name="style", referencedColumnName="Id_Style")
     * @Groups({"qst:read","qst:write","rep:read","rep:write"})
     */
    private $style;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntitule(): ?string
    {
        return $this->Intitule;
    }

    public function setIntitule(string $Intitule): self
    {
        $this->Intitule = $Intitule;

        return $this;
    }

    public function getRelation(): ?Question
    {
        return $this->relation;
    }

    public function setRelation(?Question $relation): self
    {
        $this->relation = $relation;

        return $this;
    }

    public function getStyle(): ?Style
    {
        return $this->style;
    }

    public function setStyle(?Style $style): self
    {
        $this->style = $style;

        return $this;
    }
}
