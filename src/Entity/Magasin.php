<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MagasinRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Table(name="Magasin")
 * @ORM\Entity(repositoryClass=MagasinRepository::class)
 */
class Magasin
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="Id_Magasin")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, name="Nom_Magasin")
     */
    private $Nom_Magasin;

    /**
     * @ORM\Column(type="string", length=255, name="Adresse_Magasin")
     */
    private $Adresse_Magasin;

    /**
     * @ORM\Column(type="float", name="latitude_Magasin")
     */
    private $latitude_Magasin;

    /**
     * @ORM\Column(type="float", name="longitude_Magasin")
     */
    private $longitude_Magasin;

    /**
     * @ORM\Column(type="integer", name="IdDolibarr_Magasin")
     */
    private $IdDolibarr_Magasin;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomMagasin(): ?string
    {
        return $this->Nom_Magasin;
    }

    public function setNomMagasin(string $Nom_Magasin): self
    {
        $this->Nom_Magasin = $Nom_Magasin;

        return $this;
    }

    public function getAdresseMagasin(): ?string
    {
        return $this->Adresse_Magasin;
    }

    public function setAdresseMagasin(string $Adresse_Magasin): self
    {
        $this->Adresse_Magasin = $Adresse_Magasin;

        return $this;
    }

    public function getLatitudeMagasin(): ?float
    {
        return $this->latitude_Magasin;
    }

    public function setLatitudeMagasin(float $latitude_Magasin): self
    {
        $this->latitude_Magasin = $latitude_Magasin;

        return $this;
    }

    public function getLongitudeMagasin(): ?float
    {
        return $this->longitude_Magasin;
    }

    public function setLongitudeMagasin(float $longitude_Magasin): self
    {
        $this->longitude_Magasin = $longitude_Magasin;

        return $this;
    }

    public function getIdDolibarrMagasin(): ?int
    {
        return $this->IdDolibarr_Magasin;
    }

    public function setIdDolibarrMagasin(int $IdDolibarr_Magasin): self
    {
        $this->IdDolibarr_Magasin = $IdDolibarr_Magasin;

        return $this;
    }
}
