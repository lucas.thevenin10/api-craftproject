<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\BrasserieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(
 *      normalizationContext={"groups"={"brasserie:read"}},
 *      denormalizationContext={"groups"={"brasserie:write"}}
 * )
 * @ApiFilter(
 *  SearchFilter::class,properties = {
 *   "nom":"partial"
 *  })
 * @ORM\Entity(repositoryClass=BrasserieRepository::class)
 * @ORM\Table(name="Brasserie")
 */
class Brasserie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="Id_Brasserie")
     * @Groups("biere:read","brasserie:read","brasserie:write")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, name="Nom_Brasserie")
     * @Groups("biere:read","brasserie:read","brasserie:write","tf:read", "pp:read")
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, name="NomsArtisans_Brasserie")
     * @Groups("brasserie:read","brasserie:write")
     */
    private $nomsArtisans;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, name="UrlPhotoArtisans_Brasserie")
     * @Groups("brasserie:read","brasserie:write")
     */
    private $urlPhotoArtisans;

    /**
     * @ORM\Column(type="text", nullable=true, name="Description_Brasserie")
     * @Groups("brasserie:read","brasserie:write","biere:read")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Biere::class, mappedBy="brasserie", orphanRemoval=true)
     * @Groups("brasserie:read")
     */
    private $bieres;

    /**
     * @ORM\ManyToOne(targetEntity=Origine::class, inversedBy="brasseries")
     * @ORM\JoinColumn(nullable=false, name="Id_Origine", referencedColumnName="Id_Origine")
     * @Groups("brasserie:read","brasserie:write")
     */
    private $origine;

    /**
     * @ORM\Column(type="string", length=255, name="UrlLogo_Brasserie")
     * @Groups("brasserie:read","brasserie:write","biere:read")
     */
    private $urlLogo;

    /**
     * @Groups("brasserie:read","brasserie:write")
     * @ORM\OneToMany(targetEntity=PhotoBrasserie::class, mappedBy="brasserie", cascade={"persist"})
     */
    private $photoBrasseries;

    public function __construct()
    {
        $this->bieres = new ArrayCollection();
        $this->photoBrasseries = new ArrayCollection();
    }

    public function getId() :int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getNomsArtisans(): ?string
    {
        return $this->nomsArtisans;
    }

    public function setNomsArtisans(?string $nomsArtisans): self
    {
        $this->nomsArtisans = $nomsArtisans;

        return $this;
    }

    public function getUrlPhotoArtisans(): ?string
    {
        return $this->urlPhotoArtisans;
    }

    public function setUrlPhotoArtisans(?string $urlPhotoArtisans): self
    {
        $this->urlPhotoArtisans = $urlPhotoArtisans;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Biere[]
     */
    public function getBieres(): Collection
    {
        return $this->bieres;
    }

    public function addBiere(Biere $biere): self
    {
        if (!$this->bieres->contains($biere)) {
            $this->bieres[] = $biere;
            $biere->setBrasserie($this);
        }

        return $this;
    }

    public function removeBiere(Biere $biere): self
    {
        if ($this->bieres->removeElement($biere)) {
            // set the owning side to null (unless already changed)
            if ($biere->getBrasserie() === $this) {
                $biere->setBrasserie(null);
            }
        }

        return $this;
    }

    public function getOrigine(): ?Origine
    {
        return $this->origine;
    }

    public function setOrigine(?Origine $origine): self
    {
        $this->origine = $origine;

        return $this;
    }

    public function getUrlLogo(): ?string
    {
        return $this->urlLogo;
    }

    public function setUrlLogo(string $urlLogo): self
    {
        $this->urlLogo = $urlLogo;

        return $this;
    }

    /**
     * @return Collection|PhotoBrasserie[]
     */
    public function getPhotoBrasseries(): Collection
    {
        return $this->photoBrasseries;
    }

    public function addPhotoBrasseries(PhotoBrasserie $photoBrasseries): self
    {
        if (!$this->photoBrasseries->contains($photoBrasseries)) {
            $this->photoBrasseries[] = $photoBrasseries;
            $photoBrasseries->setBrasserie($this);
        }

        return $this;
    }

    public function removePhotoBrasseries(PhotoBrasserie $photoBrasseries): self
    {
        if ($this->photoBrasseries->removeElement($photoBrasseries)) {
            // set the owning side to null (unless already changed)
            if ($photoBrasseries->getBrasserie() === $this) {
                $photoBrasseries->setBrasserie(null);
            }
        }

        return $this;
    }

}
