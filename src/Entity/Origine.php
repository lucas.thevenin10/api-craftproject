<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\OrigineRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=OrigineRepository::class)
 * @ORM\Table(name="Origine")
 */
class Origine
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="Id_Origine")
     * @Groups({"brasserie:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, name="Nom_Origine")
     * @Groups({"brasserie:read"})
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, name="UrlPhoto_Origine")
     */
    private $urlPhoto;

    /**
     * @ORM\OneToMany(targetEntity=Brasserie::class, mappedBy="origine")
     */
    private $brasseries;

    public function __construct()
    {
        $this->brasseries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getUrlPhoto(): ?string
    {
        return $this->urlPhoto;
    }

    public function setUrlPhoto(string $urlPhoto): self
    {
        $this->urlPhoto = $urlPhoto;

        return $this;
    }

    /**
     * @return Collection|Brasserie[]
     */
    public function getBrasseries(): Collection
    {
        return $this->brasseries;
    }

    public function addBrasseries(Brasserie $brasseries): self
    {
        if (!$this->brasseries->contains($brasseries)) {
            $this->brasseries[] = $brasseries;
            $brasseries->setOrigine($this);
        }

        return $this;
    }

    public function removeBrasseries(Brasserie $brasseries): self
    {
        if ($this->brasseries->removeElement($brasseries)) {
            // set the owning side to null (unless already changed)
            if ($brasseries->getOrigine() === $this) {
                $brasseries->setOrigine(null);
            }
        }

        return $this;
    }
}
