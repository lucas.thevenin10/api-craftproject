<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\StyleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=StyleRepository::class)
 * @ORM\Table(name="Style")
 */
class Style
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="Id_Style")
     * @Groups({"biere:read","rep:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, name="Nom_Style")
     * @Groups("biere:read")
     */
    private $nom;

    /**
     * @ORM\Column(type="text", name="Description_Style")
     * @Groups("biere:read")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Biere::class, mappedBy="style")
     */
    private $bieres;

    /**
     * @ORM\ManyToMany(targetEntity=Plat::class, inversedBy="styles")
     * @ORM\JoinTable(name="AccorderGeneralement",
     *    joinColumns={@ORM\JoinColumn(name="Id_Style", referencedColumnName="Id_Style")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="Id_Plat", referencedColumnName="Id_Plat")}
     *    )
     * @Groups({"biere:read"})
     */
    private $plats;

    public function __construct()
    {
        $this->bieres = new ArrayCollection();
        $this->plats = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Biere[]
     */
    public function getBieres(): Collection
    {
        return $this->bieres;
    }

    public function addBiere(Biere $biere): self
    {
        if (!$this->bieres->contains($biere)) {
            $this->bieres[] = $biere;
            $biere->setStyle($this);
        }

        return $this;
    }

    public function removeBiere(Biere $biere): self
    {
        if ($this->bieres->removeElement($biere)) {
            // set the owning side to null (unless already changed)
            if ($biere->getStyle() === $this) {
                $biere->setStyle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Plat[]
     */
    public function getPlats(): Collection
    {
        return $this->plats;
    }

    public function addPlat(Plat $plat): self
    {
        if (!$this->plats->contains($plat)) {
            $this->plats[] = $plat;
        }

        return $this;
    }

    public function removePlat(Plat $plat): self
    {
        $this->plats->removeElement($plat);

        return $this;
    }
}
