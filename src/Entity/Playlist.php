<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PlaylistRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=PlaylistRepository::class)
 * @ORM\Table(name="Playlist")
 * @ApiResource(
 *      normalizationContext={"groups"={"playlist:read"}},
 *     denormalizationContext={"groups"={"playlist:write"}}
 * )
 */
class Playlist
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="Id_Playlist")
     * @Groups({"playlist:read","user:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, name="Nom_Playlist")
     * @Groups({"playlist:read","playlist:write","user:read"})
     */
    private $nom;

    /**
     * @ORM\ManyToMany(targetEntity=Biere::class)
     * @ORM\JoinTable(name="Favoriser",
     *    joinColumns={@ORM\JoinColumn(name="Id_Playlist", referencedColumnName="Id_Playlist")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="Gencode_Biere", referencedColumnName="Gencode_Biere")}
     *    )
     * @Groups({"playlist:read","playlist:write","user:read"})
     */
    private $bieres;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class, inversedBy="playlists")
     * @ORM\JoinColumn(nullable=false, name="Id_Utilisateur", referencedColumnName="Id_Utilisateur")
     * @Groups({"playlist:read","playlist:write"})
     */
    private $utilisateur;

    public function __construct()
    {
        $this->bieres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|Biere[]
     */
    public function getBieres(): Collection
    {
        return $this->bieres;
    }

    public function addBiere(Biere $biere): self
    {
        if (!$this->bieres->contains($biere)) {
            $this->bieres[] = $biere;
        }

        return $this;
    }

    public function removeBiere(Biere $biere): self
    {
        $this->bieres->removeElement($biere);

        return $this;
    }

    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?Utilisateur $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }
}
