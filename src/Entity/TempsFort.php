<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TempsFortRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *      normalizationContext={"groups"={"tf:read"}},
 *      denormalizationContext={"groups"={"tf:write"}}
 * )
 * @ORM\Entity(repositoryClass=TempsFortRepository::class)
 * @ORM\Table(name="TempsFort")
 * 
 */
class TempsFort
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="Id_TempsFort")
     * @Groups({"tf:read","tf:write"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, name="Nom_TempsFort")
     * @Groups({"tf:read","tf:write"})
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, name="UrlLogo_TempsFort")
     * @Groups({"tf:read","tf:write"})
     */
    private $logo;

    /**
     * @ORM\Column(type="string", length=255, name="UrlImage_TempsFort")
     * @Groups({"tf:read","tf:write"})
     */
    private $image;

    /**
     * @ORM\ManyToMany(targetEntity=Biere::class)
     * @ORM\JoinTable(name="Convenir",
     *    joinColumns={@ORM\JoinColumn(name="Id_TempsFort", referencedColumnName="Id_TempsFort")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="Gencode_Biere", referencedColumnName="Gencode_Biere")}
     *    )
     * @Groups({"tf:read","tf:write"})
     */
    private $bieres;

    public function __construct()
    {
        $this->bieres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|Biere[]
     */
    public function getBieres(): Collection
    {
        return $this->bieres;
    }

    public function addBiere(Biere $biere): self
    {
        if (!$this->bieres->contains($biere)) {
            $this->bieres[] = $biere;
        }

        return $this;
    }

    public function removeBiere(Biere $biere): self
    {
        $this->bieres->removeElement($biere);

        return $this;
    }
}
