<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\SaveurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=SaveurRepository::class)
 * @ORM\Table(name="Saveur")
 */
class Saveur
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="Id_Saveur")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, name="Nom_Saveur")
     */
    private $nom;

    /**
     * @ORM\ManyToMany(targetEntity=Biere::class, mappedBy="saveurs")
     */
    private $bieres;

    public function __construct()
    {
        $this->bieres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|Biere[]
     */
    public function getBieres(): Collection
    {
        return $this->bieres;
    }

    public function addBiere(Biere $biere): self
    {
        if (!$this->bieres->contains($biere)) {
            $this->bieres[] = $biere;
            $biere->addSaveur($this);
        }

        return $this;
    }

    public function removeBiere(Biere $biere): self
    {
        if ($this->bieres->removeElement($biere)) {
            $biere->removeSaveur($this);
        }

        return $this;
    }

    public function getIntensite(): ?string
    {
        return $this->intensite;
    }

    public function setIntensite(string $intensite): self
    {
        $this->intensite = $intensite;

        return $this;
    }
}
