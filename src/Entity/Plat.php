<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PlatRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource()
 * @ORM\Table(name="Plat")
 * @ORM\Entity(repositoryClass=PlatRepository::class)
 */
class Plat
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="Id_Plat")
     * @Groups("biere:read")
     * 
     */
    private $id;

    /**
     * @Groups("biere:read")
     * @ORM\Column(type="string", length=255, name="Nom_Plat")
     */
    private $nom;

    /**
     * @Groups("biere:read")
     * @ORM\Column(type="string", length=255, name="Picto_Plat")
     */
    private $urlPicto;

    /**
     * @ORM\ManyToMany(targetEntity=Style::class, mappedBy="plats")
     */
    private $styles;

    public function __construct()
    {
        $this->styles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getUrlPicto(): ?string
    {
        return $this->urlPicto;
    }

    public function setUrlPicto(string $urlPicto): self
    {
        $this->urlPicto = $urlPicto;

        return $this;
    }

    /**
     * @return Collection|Style[]
     */
    public function getStyles(): Collection
    {
        return $this->styles;
    }

    public function addStyle(Style $style): self
    {
        if (!$this->styles->contains($style)) {
            $this->styles[] = $style;
            $style->addPlat($this);
        }

        return $this;
    }

    public function removeStyle(Style $style): self
    {
        if ($this->styles->removeElement($style)) {
            $style->removePlat($this);
        }

        return $this;
    }
}
